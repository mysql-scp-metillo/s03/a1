-- User John Smith
INSERT INTO users (email, password, datetime_created) VALUES(
	"johnsmith@gmail.com",
	"passwordA",
	"2021-01-01 01:00:00"
);

-- User Juan Dela Cruz
INSERT INTO users (email, password, datetime_created) VALUES(
	"juandelacruz@gmail.com",
	"passwordB",
	"2021-01-01 02:00:00"
);

-- User Jane Smith
INSERT INTO users (email, password, datetime_created) VALUES(
	"janesmith@gmail.com",
	"passwordC",
	"2021-01-01 03:00:00"
);

-- User Maria Dela Cruz
INSERT INTO users (email, password, datetime_created) VALUES(
	"mariadelacruz@gmail.com",
	"passwordD",
	"2021-01-01 04:00:00"
);

-- User Jone Doe
INSERT INTO users (email, password, datetime_created) VALUES(
	"johndoe@gmail.com",
	"passwordE",
	"2021-01-01 05:00:00"
);

-- Inserting Posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
	1,
	"First Code",
	"Hello World!",
	"2021-01-02 01:00:00"
);

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
	1,
	"Second Code",
	"Hello Earth!",
	"2021-01-02 02:00:00"
);

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
	2,
	"Third Code",
	"Welcome to Mars!",
	"2021-01-02 03:00:00"
);

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
	4,
	"Fourth Code",
	"Bye bye solar system!",
	"2021-01-02 04:00:00"
);

-- Selecting records
SELECT * FROM posts WHERE author_id = 1;
SELECT email, datetime_created FROM users;

-- Updating records
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Deleting records
DELETE FROM users WHERE email = "johndoe@gmail.com";
